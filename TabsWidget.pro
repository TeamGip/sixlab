QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TabsWidget
TEMPLATE = app


SOURCES += main.cpp\
        tabs.cpp

HEADERS  += tabs.h

RESOURCES += \
    src.qrc

