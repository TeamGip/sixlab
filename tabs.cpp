#include "tabs.h"
#include <QVBoxLayout>
#include <QFile>
#include <QListWidget>
#include <QListWidgetItem>

Tabs::Tabs(QWidget *parent) :
    QWidget(parent)
{
    const int ROW =3;
    const int COLUMN =3;
    predmet =QString("M;F;E").split(";");
    name =QString("Name1;Name2;Name3").split(";");
    
    QVBoxLayout *thisLay =new QVBoxLayout;
    QTableWidget *m_table =new QTableWidget();
    thisLay->addWidget(m_table);
    this->setLayout(thisLay);
    
    m_table->setColumnCount(COLUMN);
    m_table->setRowCount(ROW);
    
    m_table->setHorizontalHeaderLabels(predmet);
    m_table->setVerticalHeaderLabels(name);
    
    QFile file;
        file.setFileName(QString(":/Json.json").toLocal8Bit());
        file.open(QIODevice::ReadOnly);
    JObj =QJsonDocument::fromJson(file.readAll()).object();
    
    connect(m_table, SIGNAL(cellClicked(int,int)), this, SLOT(onShowLW(int, int)));
    
    for(int i =0;i<COLUMN;i++){
        QJsonObject JObjPr =JObj[predmet.at(i)].toObject();
        
        for(int j =0; j<ROW; j++){
            QJsonArray JArr =JObjPr[name.at(j)].toArray();
            double agr =0;
            double sum =0;
            
            foreach(QJsonValue i, JArr){
                sum+=i.toInt();
            }
            agr=sum/(JArr.size());
            m_table->setItem(j , i, new QTableWidgetItem(QString::number(agr, 'f', 2)));
        }
    }
    
    
    this->resize(QSize(400,200));
}

void Tabs::onShowLW(int row, int column)
{
    QJsonObject JObjPr =JObj[predmet.at(column)].toObject();
    QJsonArray JArr =JObjPr[name.at(row)].toArray();
    
    QListWidget *ListLoys =new QListWidget();
    foreach(QJsonValue i, JArr){
        ListLoys->addItem(QString::number(i.toInt()));
    }
    
    ListLoys->show();
    
}

