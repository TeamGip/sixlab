#ifndef TABS_H
#define TABS_H

#include <QWidget>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QStringList>

class Tabs : public QWidget
{
    Q_OBJECT
    
public:
    explicit Tabs(QWidget *parent = 0);
    QJsonObject JObj;
    QStringList name;
    QStringList predmet;
public slots:
    void onShowLW(int, int);
};

#endif // TABS_H
